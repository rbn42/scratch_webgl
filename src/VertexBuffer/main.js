 
var gl, program, unimap, attmap,gridplane;
currentAngle = -30;
incAngle = 0.5;
var frameCounter = [];
for(var i=0;i<120;i++)
	frameCounter.push(0); 

var eyePosition=[0, 0, 7];
var worldMatrix= new Float32Array(16);
var normalMatrix= new Float32Array(9);
var viewMatrix= new Float32Array(16);
var projectMatrix= new Float32Array(16);
var canvas ;
	
init();
animate();
//setTimeout(update, 1000 / 60);
var updateInterval = 1 / 60;
setTimeout(__update, updateInterval);

function init() {
	  canvas = document.getElementById('my_canvas');
	var vertexShaderSrc = document.getElementById('vshader').textContent;
	var fragmentShaderSrc = document.getElementById('fshader').textContent;
	gl = getGL(canvas);
	program = gl_build(gl, vertexShaderSrc, fragmentShaderSrc);
	gl.useProgram(program);
	unimap = gl_uniformLocations(gl, program);
	attmap = gl_attributeLocations(gl, program);
	gl.uniform3fv(unimap["lightDir"], [0, 0, 1]);
	gl.uniform3fv(unimap["u_lightSource0"], [0, 0, 1]);
	gl.uniform1f(unimap["u_lightSource0Strenth"], 1);
	gl.uniform3fv(unimap["u_lightSource1"], [0, 0, -3]);
	gl.uniform3fv(unimap["u_eyePosition"], eyePosition);

	// Load an image to use. Returns a WebGLTexture object
	spiritTexture = loadImageTexture(gl, "spirit.png");
  
	var obj=obj_gridplane([20,20],.01);
	gridplane=obj;
	// Set up all the vertex attributes for vertices, normals and texCoords
	gl_bindAttributeBuffer(gl, attmap["a_normal"], obj.n, 3);
	vbuffer=gl_bindAttributeBuffer(gl, attmap["a_position"],obj.v, 3);
	gl_bindAttributeBuffer(gl, attmap["a_texCoord"], obj.t, 2);

	gl_bindIndexObjectBuffer(gl, obj.i);
 
	// gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, g.box.indexObject);

} 
function draw() {

	// Clear the canvas
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.uniform3fv(unimap["u_ambientColor"], [.2,.2,.2]);
	// Make a worldMatrix
	mat4.identity(worldMatrix);
	 mat4.rotate(worldMatrix, worldMatrix, 3 / 180 * Math.PI, [1, 0, 0]); 
	mat4.rotate(worldMatrix, worldMatrix, currentAngle / 180 * Math.PI, [0, 1, 0]);
 	mat4.scale(worldMatrix,worldMatrix,[2,2,2]);
	gl.uniformMatrix4fv(unimap["u_worldMatrix"], gl.FALSE, worldMatrix); 
	// Make a normalMatrix
	mat3.normalFromMat4(normalMatrix, worldMatrix)  ;
	gl.uniformMatrix3fv(unimap["u_normalMatrix"], gl.FALSE, normalMatrix);
	// Make a viewMatrix
	mat4.lookAt(viewMatrix, eyePosition, [0, 0, 0], [0, 1, 0]);
	gl.uniformMatrix4fv(unimap["u_viewMatrix"], gl.FALSE, viewMatrix);
	// Make a projectMatrix
	mat4.perspective(projectMatrix, 30 / 180 * Math.PI, canvas.clientWidth / canvas.clientHeight, 1, 10000); 
	gl.uniformMatrix4fv(unimap["u_projectMatrix"], gl.FALSE, projectMatrix);
 

	// Bind the texture to use
	gl.bindTexture(gl.TEXTURE_2D, spiritTexture);

	// Draw the cube
	gl.drawElements(gl.TRIANGLES,  gridplane.i.length, gl.UNSIGNED_SHORT, 0);
	//gl.drawElements(gl.TRIANGLES, g.box_mesh_length, gl.UNSIGNED_BYTE, 0);
	return;
}


var tobeUpdateTime = 0;
var lastUpdateTime = Date.now();
function __update() {
	var temp = Date.now();
	tobeUpdateTime += temp - lastUpdateTime;
	lastUpdateTime = temp;
	while (tobeUpdateTime > updateInterval * 1000) {
		tobeUpdateTime -= updateInterval * 1000;
		update(updateInterval);
	}
	setTimeout(__update, updateInterval);
}
function update(intv) {
	
	fps(); 
	randomdepth(); 
	currentAngle=9; 
}
function randomdepth(){
	var p=[Math.random(),Math.random()]; 
	var position =new Int8Array(2);
	vec2.multiply(position,p,gridplane.s);
	var depth=(Math.random()-1/2)*.031;
	var l=[]; 
	if(position[0]>0)
		l.push(position[0]-1+position[1]*gridplane.s[0]);
	if(position[1]>0)
		l.push(position[0]+(position[1]-1)*gridplane.s[0]);
	if(position[0]<gridplane.s[0]-1)
		l.push(position[0]+1+position[1]*gridplane.s[0]);
	if(position[1]<gridplane.s[1]-1)
		l.push(position[0]+(position[1]+1)*gridplane.s[0]); 
	
	var _index= (position[0]+position[1]*gridplane.s[0]);
	
	 l.push(_index);
	
	var d=0;
	for( i in l)
		d+=gridplane.d[l[i]]; 
	d/=l.length;
	d+=depth;
	depth=d; 
	
	
	obj_gridplane_set(gl, vbuffer, gridplane, position, depth);
	
}

function fps() {
	if(frameCounter[0]){ 
		var t=frameCounter.length;
		var f=1000*t/(frameCounter[t-1]-frameCounter[0]);
		consoleinfo("<table><tr><td>fps</td><td>{0}</td>".format(Math.floor(f))); 
	} 
}

function consoleinfo(s) {
	$("div").html(s);
}

function animate(time) {
	draw(); 
	frameCounter.shift();
	frameCounter.push(time);
	requestAnimationFrame(animate);
};

